#!/bin/bash
echo "Setting environment variables... "
if [ ! -f "./.env" ]; then
  cat ./.env.dist > .env
fi
echo "Done"
echo "Creating config files... "
for f in $(ls ./app/protected/config/*.dist)
do
  if [ ! -f "${f%.dist}" ]; then
    cat $f > ${f%.dist}
  fi
done
echo "Done"
echo "Starting docker containers"
docker-compose up -d
echo "Loading database"
sleep 15 #wait for mysql to get up
docker exec salestracker-app-mysql bash -c 'mysql -h localhost -u "$MYSQL_USER" -p"$MYSQL_PASSWORD" "$MYSQL_DATABASE" < /application/app/protected/data/schema.mysql.sql'
echo "Installing dependencies"
docker exec salestracker-app-php-fpm bash -c "cd /application; composer install && composer -o dump-autoload"
