<?php

use Psr\Container\ContainerInterface;
use SalestrackerApp\Extension\Importer\SpreadsheetUserImporter;
use SalestrackerApp\Extension\Importer\UserImporter;
use SalestrackerApp\Extension\Authorization\ActiveRecordUserProvider;
use SalestrackerApp\Extension\Authorization\UserAuthentication;
use SalestrackerApp\Extension\Authorization\UserProvider;
use SalestrackerApp\Extension\Upload\FileUploader;
use SalestrackerApp\Extension\Util\ParamFetcher;

return array(
    FileUploader::class => DI\autowire(FileUploader::class)
        ->constructorParameter(0, Yii::app()->params['uploadFileDestination'])
        ->constructorParameter(1, Yii::app()->params['maxUploadedFileSize']),
    UserProvider::class => DI\autowire(ActiveRecordUserProvider::class)
        ->constructorParameter(0, DI\autowire(User::class)->constructorParameter(0, null)),
    UserAuthentication::class => DI\autowire(UserIdentity::class),
    Swift_Mailer::class => function (ContainerInterface $container) {
        $paramFetcher = $container->get(ParamFetcher::class);
        $mailConfig = $paramFetcher->get('mail');
        $transportType = $mailConfig['transport'] ?? '';
        switch ($transportType) {
            case 'smtp':
                $username = $mailConfig['username'];
                $password = $mailConfig['password'];
                $transport = (new Swift_SmtpTransport(
                    $mailConfig['host'],
                    $mailConfig['port'],
                    $mailConfig['ssl'] ? 'ssl' : null
                ))
                    ->setUsername($username)
                    ->setPassword($password);

                return new Swift_Mailer($transport);
            case 'sendmail':
                $transport = new Swift_SendmailTransport($mailConfig['command']);

                return new Swift_Mailer($transport);
        }

        throw new InvalidArgumentException('Invalid mail transport type');

    },
    UserImporter::class => DI\autowire(SpreadsheetUserImporter::class),
);
