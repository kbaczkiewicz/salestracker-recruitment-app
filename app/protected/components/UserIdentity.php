<?php

use SalestrackerApp\Extension\Authorization\UserAuthentication;
use SalestrackerApp\Extension\Authorization\UserProvider;

/**
 * UserIdentity represents the data needed to identity a user.
 * It contains the authentication method that checks if the provided
 * data can identity the user.
 */
class UserIdentity extends CUserIdentity implements UserAuthentication
{
    private $userProvider;

    public function __construct(UserProvider $userProvider)
    {
        $this->userProvider = $userProvider;
    }

    public function doAuthenticate(string $username, string $password): array
    {
        $this->username = $username;
        $this->password = $password;
        return $this->authenticate();
    }

    public function authenticate(): array
    {
        $user = $this->userProvider->getByUsername($this->username);
        if (null === $user) {
            return ['username' => ['User with this username does not exist']];
        }

        if (sha1($this->password) !== $user->password) {
            return ['password' => ['Password is incorrect']];
        }

        $user->validate('last_password_change_date');

        return $user->getErrors();
    }
}
