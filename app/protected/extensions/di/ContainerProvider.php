<?php

use DI\Container;

/**
 * @property $definitions
 */
class ContainerProvider extends \CComponent
{
    public $definitionsPath;
    private $container;

    public function init(): void
    {
        $builder = new \DI\ContainerBuilder();
        $builder->enableCompilation(__DIR__ . '/../../cache');
        $builder->writeProxiesToFile(true, __DIR__ . '/../../cache/proxies');
        $builder->addDefinitions($this->getDefinitions());
        $this->container = $builder->build();
    }

    public function getContainer(): Container
    {
        return $this->container;
    }

    private function getDefinitions()
    {
        $definitions = require_once($this->definitionsPath);

        return $definitions;
    }
}
