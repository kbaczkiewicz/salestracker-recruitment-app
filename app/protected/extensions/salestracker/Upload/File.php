<?php


namespace SalestrackerApp\Extension\Upload;


class File
{
    private $name;
    private $size;
    private $extension;

    public function __construct(string $name, string $size, string $extension)
    {
        $this->name = $name;
        $this->size = $size;
        $this->extension = $extension;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getSize(): string
    {
        return $this->size;
    }

    public function getExtension(): string
    {
        return $this->extension;
    }
}
