<?php


namespace SalestrackerApp\Extension\Upload;


use SalestrackerApp\Extension\Upload\Exception\FileUploadException;

class FileUploader
{
    private $maxSize;
    private $mimeTypes;
    private $destination;

    public function __construct(string $destination, int $maxSize)
    {
        $this->maxSize = $maxSize;
        $this->destination = $destination;
    }


    public function upload(array $fileData): string
    {
        $finfo = new \finfo(FILEINFO_MIME_TYPE);
        $fileName = $fileData['name']['spreadsheet'];
        $extension = pathinfo($fileName)['extension'];
        $size = $fileData['size']['spreadsheet'];
        $tmpName = $fileData['tmp_name']['spreadsheet'];
        $type = $finfo->file($tmpName);
        $this->checkMaxSize($size);
        $this->checkMimeType($type);
        $newFileName = md5(microtime());
        $result = move_uploaded_file($tmpName, sprintf("%s/%s.%s", $this->destination, $newFileName, $extension));
        if (false === $result) {
            throw new FileUploadException('Error while uploading file. Check if target folder exists and if you have permissions to write to it');
        }

        return $newFileName . '.' . $extension;
    }

    public function changeMaxSizeLimit(int $maxSize): void
    {
        $this->maxSize = $maxSize;
    }

    public function setMimeTypes(array $mimeTypes): void
    {
        $this->mimeTypes = $mimeTypes;
    }

    private function getMaxSize(): int
    {
        return $this->maxSize ?? 4194304;
    }

    private function getMimeTypes()
    {
        return $this->mimeTypes ?? [];
    }

    private function checkMaxSize(int $size)
    {
        if ($size > $this->getMaxSize()) {
            throw new FileUploadException('File is too large');
        }
    }

    private function checkMimeType(string $type)
    {
        if (false === in_array($type, $this->getMimeTypes())) {
            throw new FileUploadException('File type not supported');
        }
    }

}
