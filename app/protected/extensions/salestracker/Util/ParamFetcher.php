<?php


namespace SalestrackerApp\Extension\Util;


class ParamFetcher
{
    public function get(string $name)
    {
        return \Yii::app()->params[$name] ?? null;
    }
}
