<?php


namespace SalestrackerApp\Extension\Importer;

use SalestrackerApp\Extension\Authorization\UserAuthentication;
use SalestrackerApp\Extension\Authorization\Util\PasswordUtil;
use SalestrackerApp\Extension\Mailing\MailSender;
use SalestrackerApp\Extension\Spreadsheet\Model\User;
use SalestrackerApp\Extension\Spreadsheet\UserSpreadsheetHandler;
use SalestrackerApp\Extension\Util\ParamFetcher;

class SpreadsheetUserImporter implements UserImporter
{
    private $authentication;
    private $passwordUtil;
    private $mailer;
    private $paramFetcher;
    private $spreadsheetHandler;

    public function __construct(
        UserAuthentication $authentication,
        PasswordUtil $passwordUtil,
        MailSender $mailer,
        ParamFetcher $paramFetcher,
        UserSpreadsheetHandler $spreadsheetHandler
    ) {
        $this->authentication = $authentication;
        $this->passwordUtil = $passwordUtil;
        $this->mailer = $mailer;
        $this->paramFetcher = $paramFetcher;
        $this->spreadsheetHandler = $spreadsheetHandler;
    }

    public function import(string $path)
    {
        $this->spreadsheetHandler->openSheet($path);
        foreach ($this->spreadsheetHandler->getData() as $row => $userData) {
            $user = new \User();
            $password = $this->passwordUtil->generatePassword();
            $user->attributes = $this->prepareAttributes($userData, $password);

            $this->save($user, $password, $row);
        }
    }

    private function sendAccountCreatedMail(\User $user, string $password): void
    {
        $this->mailer->sendCredentialsMail($user->email, ['username' => $user->username, 'password' => $password]);
    }

    private function markAsUnsaved(int $row): void
    {
        $this->spreadsheetHandler->writeToSheet('E'.$row, 'Unsaved');
    }

    private function prepareAttributes(?User $userData, string $password): array
    {
        if (null === $userData) {
            return [];
        }

        return array_merge(
            $userData->jsonSerialize(),
            [
                'password' => $this->passwordUtil->hashPassword($password),
                'last_password_change_date' => (new \DateTime())->format('Y-m-d'),
                'created_at' => (new \DateTime())->format('Y-m-d H:i:s'),
            ]
        );
    }

    private function save(\User $user, string $password, int $row): void
    {
        try {
            $user->validate();
            if (false === $user->hasErrors()) {
                $user->save();
                $this->sendAccountCreatedMail($user, $password);
            } else {
                $this->markAsUnsaved($row + 1);
            }
        } catch (\CDbException $e) {
            $this->markAsUnsaved($row + 1);
        }
    }
}
