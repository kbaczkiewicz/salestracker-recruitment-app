<?php


namespace SalestrackerApp\Extension\Importer;


interface UserImporter
{
    public function import(string $path);
}
