<?php


namespace SalestrackerApp\Extension\Authorization;


interface UserProvider
{
    public function get(int $id): ?\User;
    public function getByUsername(string $username): ?\User;
}
