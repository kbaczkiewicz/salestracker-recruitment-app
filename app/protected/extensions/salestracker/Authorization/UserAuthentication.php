<?php


namespace SalestrackerApp\Extension\Authorization;


interface UserAuthentication
{
    public function doAuthenticate(string $username, string $password): array;
}
