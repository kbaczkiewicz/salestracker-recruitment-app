<?php


namespace SalestrackerApp\Extension\Authorization\Util;


use SalestrackerApp\Extension\Authorization\UserProvider;

class PasswordUtil
{
    private $userProvider;

    public function __construct(UserProvider $userProvider)
    {
        $this->userProvider = $userProvider;
    }

    public function hashPassword(string $password): string
    {
        return sha1($password);
    }

    public function generatePassword(): string
    {
        return substr(str_shuffle(md5(microtime())), 0, 8);
    }

    public function changePassword(int $userId, \PasswordChangeForm $passwordChangeForm): void
    {
        $user = $this->userProvider->get($userId);
        $this->checkPasswords($user, $passwordChangeForm);
        if (empty($passwordChangeForm->getErrors())) {
            $user->password = $this->hashPassword($passwordChangeForm->newPassword);
            $user->last_password_change_date = (new \DateTime())->format('Y-m-d');
            $user->save();
        }
    }

    private function checkPasswords(\User $user, \PasswordChangeForm $passwordChangeForm)
    {
        $passwordChangeForm->validate();

        if (null === $user || $this->hashPassword($passwordChangeForm->oldPassword) !== $user->password) {
            $passwordChangeForm->addError('oldPassword', 'Old password is incorrect');
        }
    }
}
