<?php


namespace SalestrackerApp\Extension\Authorization;


class ActiveRecordUserProvider implements UserProvider
{
    private $model;

    public function __construct(\User $user)
    {
        $this->model = $user::model();
    }

    public function get(int $id): ?\User
    {
        return $this->model->findByPk($id);
    }


    public function getByUsername(string $username): ?\User
    {
        return $this->model->find('username = :username', [':username' => $username]);
    }
}
