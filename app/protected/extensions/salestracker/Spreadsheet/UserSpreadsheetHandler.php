<?php


namespace SalestrackerApp\Extension\Spreadsheet;


use PhpOffice\PhpSpreadsheet\Shared\Date;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;
use SalestrackerApp\Extension\Spreadsheet\Model\User;

class UserSpreadsheetHandler extends SpreadsheetHandler
{
    protected function getAttributesMap(): array
    {
        return [
            'A' => 'name',
            'B' => 'surname',
            'C' => 'email',
            'D' => 'date_of_birth',
        ];
    }

    protected function getAttributes(Worksheet $worksheet, int $row)
    {
        $attributes = [];
        foreach ($this->getAttributesMap() as $sheetKey => $attribute) {
            $attributes[$attribute] = $this->getAttribute($worksheet, $attribute, $sheetKey.$row);
        }

        return $attributes;
    }

    /**
     * @return User[]
     */
    public function getData(int $beginRow = 1): array
    {
        return array_map(
            function (array $data) {
                try {
                    return new User($data['name'], $data['surname'], $data['email'], $data['date_of_birth']);
                } catch (\TypeError $e) {
                    return null;
                }
            },
            $this->readData($beginRow)
        );
    }

    private function getAttribute(Worksheet $worksheet, string $attribute, string $column)
    {
        try {
            if ('date_of_birth' === $attribute) {
                $value = $worksheet->getCell($column)->getCalculatedValue();

                return null !== $value ? (Date::excelToDateTimeObject($value)) : null;
            } else {
                return (string)$worksheet->getCell($column)->getValue();
            }
        } catch (\Exception $e) {
            return null;
        }

    }
}
