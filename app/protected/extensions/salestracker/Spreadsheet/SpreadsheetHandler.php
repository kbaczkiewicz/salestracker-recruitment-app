<?php


namespace SalestrackerApp\Extension\Spreadsheet;


use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;

abstract class SpreadsheetHandler
{
    /** @var Spreadsheet */
    protected $sheet;
    protected $path;

    public function openSheet(string $path): void
    {
        $reader = IOFactory::createReader(ucfirst(pathinfo($path)['extension']));
        $reader->setReadDataOnly(true);
        $this->sheet = $reader->load($path);
        $this->path = $path;
    }

    public function writeToSheet(string $column, string $value)
    {
        $worksheet = $this->sheet->setActiveSheetIndex(0);
        $worksheet->setCellValue($column, $value);
        $writer = IOFactory::createWriter($this->sheet, ucfirst(pathinfo($this->path)['extension']));
        $writer->save($this->path);
    }

    protected function readData(int $beginRow = 1)
    {
        $data = [];
        $worksheet = $this->sheet->getActiveSheet();
        for ($row = $beginRow; ; $row++) {
            if (false === $this->dataExists($worksheet, $row)) {
                break;
            }

            $data[] = $this->getAttributes($worksheet, $row);
        }

        return $data;
    }

    protected function getAttributes(Worksheet $worksheet, int $row)
    {
        $attributes = [];
        foreach ($this->getAttributesMap() as $sheetKey => $attribute) {
            $value = (string)$worksheet->getCell($sheetKey.$row)->getValue();
            $attributes[$attribute] = $value;
        }

        return $attributes;
    }

    private function dataExists(Worksheet $worksheet, int $row): bool
    {
        foreach (array_keys($this->getAttributesMap()) as $field) {
            if (null !== $worksheet->getCell($field.$row)->getValue()) {
                return true;
            }
        }

        return false;
    }

    abstract public function getData(int $beginRow = 1): array;
    abstract protected function getAttributesMap(): array;
}
