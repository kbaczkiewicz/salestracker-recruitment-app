<?php


namespace SalestrackerApp\Extension\Spreadsheet\Model;


class User implements \JsonSerializable
{
    private $name;
    private $surname;
    private $email;
    private $dateOfBirth;

    public function __construct(string $name, string $surname, string $email, \DateTime $dateOfBirth)
    {
        $this->name = $name;
        $this->surname = $surname;
        $this->email = $email;
        $this->dateOfBirth = $dateOfBirth;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getSurname(): string
    {
        return $this->surname;
    }

    public function getEmail(): string
    {
        return $this->email;
    }

    public function getDateOfBirth(): \DateTime
    {
        return $this->dateOfBirth;
    }

    public function jsonSerialize()
    {
        return [
            'username' => $this->email,
            'name' => $this->name,
            'surname' => $this->surname,
            'email' => $this->email,
            'date_of_birth' => $this->dateOfBirth->format('Y-m-d'),
        ];
    }
}
