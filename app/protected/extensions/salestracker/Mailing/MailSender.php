<?php


namespace SalestrackerApp\Extension\Mailing;


use SalestrackerApp\Extension\Util\ParamFetcher;
use Swift_Message;
use Yii;

class MailSender
{
    private $swiftMailer;
    private $paramFetcher;

    public function __construct(\Swift_Mailer $swiftMailer, ParamFetcher $paramFetcher)
    {
        $this->swiftMailer = $swiftMailer;
        $this->paramFetcher = $paramFetcher;
    }

    public function sendPasswordChangeMail(string $recipient, array $params)
    {
        try {
            $message = $this->createMailMessage($recipient);
            $message->setBody($this->getPasswordResetMailTemplate($params['username'], $params['url']), 'text/html');
            $this->swiftMailer->send($message);
        } catch (\Exception $e) {
            Yii::log($e->getMessage(), 'error', 'application');
            Yii::log($message->getBody(), 'info', 'application');
        }
    }

    public function sendCredentialsMail(string $recipient, array $params)
    {
        try {
            $message = $this->createMailMessage($recipient);
            $message->setBody($this->getCredentialsMailTemplate($params['username'], $params['password']), 'text/html');
            $this->swiftMailer->send($message);
        } catch (\Exception $e) {
            Yii::log($e->getMessage(), 'error', 'application');
            Yii::log($message->getBody(), 'info', 'application');
        }
    }

    private function getPasswordResetMailTemplate(string $username, string $url): string
    {
        return sprintf(
            'Hello %s,<br /><br />Your password has expired.<br /><br />'
            .'You need to change it using <a href="%s">this link</a>.<br />',
            $username,
            $url
        );
    }

    private function getCredentialsMailTemplate(string $username, string $password): string
    {
        return sprintf(
            'Hello %s,<br /><br />Your account in our app has been created and activated.<br /><br />'
            .'Your credentials are:<br />Username: %s<br />Password:%s<br />',
            $username,
            $username,
            $password
        );
    }

    private function createMailMessage(string $recipient): Swift_Message
    {
        $mailConfig = $this->paramFetcher->get('mail');
        $message = new Swift_Message('Account register');
        $message
            ->setFrom([$mailConfig['senderMail'] => $mailConfig['senderName']])
            ->setTo([$recipient]);

        return $message;
    }
}
