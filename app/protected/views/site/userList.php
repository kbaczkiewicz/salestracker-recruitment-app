<?php

$this->pageTitle = Yii::app()->name.' - User list';
$this->breadcrumbs = array(
    'User list',
);

Yii::app()->clientScript->registerScript(
    'search',
    "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#user-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
"
);
?>

<h1>Show users</h1>
<?php echo CHtml::link('Advanced Search', '#', array('class' => 'search-button')); ?>
<div class="search-form" style="display:none">
    <?php $this->renderPartial(
        '_search',
        array(
            'model' => $model,
        )
    ); ?>
</div><!-- search-form -->

<?php $this->widget(
    'zii.widgets.grid.CGridView',
    array(
        'id' => 'user-grid',
        'dataProvider' => $model->search(),
        'filter' => $model,
        'columns' => array(
            'id',
            'username',
            'name',
            'surname',
            'email',
            'created_at',
            array(
                'class' => 'CButtonColumn',
                'template' => '{show}',
                'buttons' => [
                        'show' => [
                                'label' => 'Show',
                                'url'=>'Yii::app()->createUrl("site/show", array("userId"=>$data->id))',

                        ]
                ]
		),
        ),
    )
); ?>
