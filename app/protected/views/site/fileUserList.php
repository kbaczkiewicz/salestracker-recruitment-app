<?php
$this->pageTitle=Yii::app()->name . ' - File user list';
$this->breadcrumbs=array(
    'File user list',
);
?>

<h1>File user list</h1>

<table class="items">
    <tbody>
    <?php if (count($users)): ?>
    <?php foreach ($users as $row => $user):
    ?>
        <tr>
            <td><?= $user->getEmail() ?></td>
            <td><?= $user->getName() ?></td>
            <td><?= $user->getSurname() ?></td>
            <td><?= $user->getDateOfBirth()->format('Y-m-d') ?></td>
        </tr>
    <?php endforeach; ?>
    <?php else: ?>
    <p>User has no users in his file</p>
    <?php endif; ?>
    </tbody>
</table>
