<?php
/* @var $this SiteController */
/* @var $model UserFileForm */
/* @var $form CActiveForm  */

$this->pageTitle=Yii::app()->name . ' - Upload';
$this->breadcrumbs=array(
    'Upload',
);
?>

<h1>Upload</h1>

<div class="form">
    <?php $form=$this->beginWidget('CActiveForm', array(
        'id'=>'login-form',
        'enableClientValidation'=>true,
        'clientOptions'=>array(
            'validateOnSubmit'=>true,
        ),
        'htmlOptions'=>array('enctype'=>'multipart/form-data'),
    )); ?>

    <p class="note">Fields with <span class="required">*</span> are required.</p>

    <div class="row">
        <?php echo $form->labelEx($model,'spreadsheet'); ?>
        <?php echo $form->fileField($model,'spreadsheet'); ?>
        <?php echo $form->error($model,'spreadsheet'); ?>
    </div>

    <div class="row buttons">
        <?php echo CHtml::submitButton('Upload'); ?>
    </div>

    <?php $this->endWidget(); ?>
</div><!-- form -->
