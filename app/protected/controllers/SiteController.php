<?php

use SalestrackerApp\Extension\Authorization\Util\PasswordUtil;
use SalestrackerApp\Extension\Importer\UserImporter;
use SalestrackerApp\Extension\Authorization\UserAuthentication;
use SalestrackerApp\Extension\Authorization\UserProvider;
use SalestrackerApp\Extension\Mailing\MailSender;
use SalestrackerApp\Extension\Spreadsheet\UserSpreadsheetHandler;
use SalestrackerApp\Extension\Upload\FileUploader;
use SalestrackerApp\Extension\Util\ParamFetcher;

class SiteController extends Controller
{
    /**
     * Declares class-based actions.
     */
    public function actions()
    {
        return array(
            // captcha action renders the CAPTCHA image displayed on the contact page
            'captcha' => array(
                'class' => 'CCaptchaAction',
                'backColor' => 0xFFFFFF,
            ),
            // page action renders "static" pages stored under 'protected/views/site/pages'
            // They can be accessed via: index.php?r=site/page&view=FileName
            'page' => array(
                'class' => 'CViewAction',
            ),
        );
    }

    public function accessRules()
    {
        return array(
            array(
                'allow',  // allow all users to perform 'index' and 'view' actions
                'actions' => array('login', 'passwordChange'),
                'users' => array('*'),
            ),
            array(
                'allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions' => array('index', 'upload', 'logout', 'page', 'show'),
                'users' => array('@'),
            ),
            array(
                'deny',  // deny all users
                'users' => array('*'),
            ),
        );
    }

    public function filters()
    {
        return ['accessControl'];
    }

    /**
     * This is the default 'index' action that is invoked
     * when an action is not explicitly requested by users.
     */
    public function actionIndex()
    {
        $this->actionShow();
    }

    /**
     * This is the action to handle external exceptions.
     */
    public function actionError()
    {
        if ($error = Yii::app()->errorHandler->error) {
            if (Yii::app()->request->isAjaxRequest) {
                echo $error['message'];
            } else {
                $this->render('error', $error);
            }
        }
    }

    public function actionShow()
    {
        /** @var CHttpRequest $request */
        $request = Yii::app()->request;
        if ($request->getQuery('userId', null)) {
            $users = $this->getUsersFromFile();

            $this->render('fileUserList', ['users' => $users]);

        } else {
            $model = new User('search');
            $model->unsetAttributes();  // clear any default values
            if (isset($_GET['User'])) {
                $model->attributes = $_GET['User'];
            }

            $this->render('userList', ['model' => $model]);
        }
    }

    public function actionUpload()
    {

        $model = new UserFileForm();
        if (isset($_FILES['UserFileForm'])) {
            $importer = $this->getContainer()->get(UserImporter::class);
            $fileUploader = $this->getContainer()->get(FileUploader::class);
            $userProvider = $this->getContainer()->get(UserProvider::class);
            $fileUploader->setMimeTypes(
                [
                    'application/vnd.ms-excel',
                    'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
                    'application/vnd.oasis.opendocument.spreadsheet',
                    'application/vnd.openxmlformats-officedocument.spreadsheetml.sheetapplication/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
                ]
            );

            $filename = $fileUploader->upload($_FILES['UserFileForm']);
            $userFile = new UserFile();
            $userFile->file = $filename;
            $userFile->user_id = $userProvider->getByUsername(Yii::app()->user->id)->id;
            $importer->import(
                $this->getContainer()->get(ParamFetcher::class)->get('uploadFileDestination') . '/' .  $filename
            );

            $userFile->save();

            return $this->redirect(Yii::app()->homeUrl);
        }

        $this->render('upload', ['model' => $model]);
    }

    public function actionLogin()
    {
        $model = new LoginForm;
        /** @var CHttpRequest $request */
        $request = Yii::app()->request;
        if ($request->getParam('ajax', null) === 'login-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }

        if ($request->getParam('LoginForm', null)) {
            $model->attributes = $request->getParam('LoginForm', null);
            if ($model->validate()) {
                $userIdentity = $this->getContainer()->get(UserAuthentication::class);
                $errors = $userIdentity->doAuthenticate($model->username, $model->password);
                if (empty($errors)) {
                    Yii::app()->user->login($userIdentity, (int)($model->rememberMe ?? 0) * 3600 * 24 * 30);
                    $this->redirect(Yii::app()->user->returnUrl);
                }

                $model->addErrors($errors);
                $expiredPasswordError = $model->getErrors('last_password_change_date');
                if (false === empty($expiredPasswordError)) {
                    $userProvider = $this->getContainer()->get(UserProvider::class);
                    $user = $userProvider->getByUsername($model->username);
                    $this->sendChangePasswordMail($user, $this->createPasswordChangeCode($user->id));
                }
            }
        }

        $this->render('login', array('model' => $model));
    }

    /**
     * Logs out the current user and redirect to homepage.
     */
    public function actionLogout()
    {
        Yii::app()->user->logout();
        $this->redirect(Yii::app()->homeUrl);
    }

    public function actionPasswordChange()
    {
        /** @var CHttpRequest $request */
        $form = new PasswordChangeForm();
        $request = Yii::app()->request;
        $code = (new PasswordChangeCode())->find('code = :code', [':code' => $request->getQuery('code', null)]);
        if (null === $code) {
            return $this->redirect(Yii::app()->homeUrl);
        }

        if (null !== $request->getParam('PasswordChangeForm', null)) {
            $form->attributes = $request->getParam('PasswordChangeForm');
            $passwordUtil = $this->getContainer()->get(PasswordUtil::class);
            $passwordUtil->changePassword($code->user_id, $form);
            if (empty($form->getErrors())) {
                $code->delete();
                return $this->redirect(Yii::app()->homeUrl . '?r=site/login');
            }
        }

        return $this->render('passwordChange', array('model' => $form));
    }

    private function sendChangePasswordMail(\User $user, PasswordChangeCode $code)
    {
        $mailer = $this->getContainer()->get(MailSender::class);
        $mailer->sendPasswordChangeMail($user->email,
            [
                'username' => $user->username,
                'url' => 'http://' . Yii::app()->params['baseUrl'].'?r=site/passwordChange&code='.$code->code,
            ]
        );
    }

    private function createPasswordChangeCode(int $userId): PasswordChangeCode
    {
        $code = new PasswordChangeCode();
        $code->attributes = ['user_id' => $userId, 'code' => sha1(microtime())];
        $code->save();

        return $code;
    }

    private function getUsersFromFile(): array
    {
        $request = Yii::app()->request;
        $userFiles = (new UserFile())->findAll('user_id = :userid', ['userid' => $request->getQuery('userId')]);
        if (false === empty($userFiles)) {
            return $this->getDataFromSheets($userFiles);
        } else {
            return [];
        }
    }

    private function getDataFromSheets(array $userSheets)
    {
        $data = [];
        $uploadDirectory = Yii::app()->params['uploadFileDestination'];
        $spreadsheetHandler = $this->getContainer()->get(UserSpreadsheetHandler::class);
        foreach ($userSheets as $userSheet) {
            $spreadsheetHandler->openSheet($uploadDirectory.'/'.$userSheet->file);
            $data = array_merge($data, array_filter($spreadsheetHandler->getData()));
        }
        return $data;
    }
}
