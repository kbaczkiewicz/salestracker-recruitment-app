SET NAMES utf8;

CREATE TABLE tbl_user (
    id INTEGER NOT NULL PRIMARY KEY AUTO_INCREMENT,
    username VARCHAR(128) NOT NULL UNIQUE,
    name VARCHAR(128) NOT NULL,
    surname VARCHAR(128) NOT NULL,
    password VARCHAR(128) NOT NULL,
    email VARCHAR(128) NOT NULL UNIQUE,
    date_of_birth DATE,
    last_password_change_date DATE,
    created_at DATE
) ENGINE=InnoDB DEFAULT CHARACTER SET=utf8;

INSERT INTO tbl_user SET username = 'kbaczkiewicz', `name` = 'Kamil', surname = 'Bączkiewicz', password = sha1('1234qwer'), email='kbaczkiewitz@example.com', date_of_birth='1996-05-14', last_password_change_date=CURDATE(), created_at=CURDATE();
INSERT INTO tbl_user SET username = 'expired', `name` = 'Test', surname = 'Nieaktywny', password = sha1('1234qwer'), email='expiredtest@example.com', date_of_birth='1996-05-14', last_password_change_date='1970-01-01', created_at=CURDATE();

CREATE TABLE tbl_user_file (
    id INTEGER NOT NULL PRIMARY KEY AUTO_INCREMENT,
    file VARCHAR(64) NOT NULL,
    user_id INTEGER NOT NULL,
    FOREIGN KEY user_id_file_id (user_id)
    REFERENCES tbl_user(id)
    ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARACTER SET=utf8;

CREATE TABLE tbl_password_change_code (
  id INTEGER NOT NULL PRIMARY KEY AUTO_INCREMENT,
  user_id INTEGER NOT NULL,
  code VARCHAR(64),
  FOREIGN KEY user_id_code_id (user_id)
    REFERENCES tbl_user(id)
    ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARACTER SET=utf8;
