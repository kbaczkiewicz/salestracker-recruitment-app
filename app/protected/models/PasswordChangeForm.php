<?php


class PasswordChangeForm extends CFormModel
{
    public $oldPassword;
    public $newPassword;
    public $confirmPassword;

    public function rules()
    {
        return [
            ['oldPassword, newPassword, confirmPassword', 'required'],
            ['newPassword', 'length', 'min' => 8],
            ['confirmPassword', 'samePasswords'],
        ];
    }

    public function samePasswords()
    {
        if ($this->newPassword !== $this->confirmPassword) {
            $this->addError('newPassword', 'Passwords are not identical');
        }
    }
}
