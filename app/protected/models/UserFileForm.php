<?php


class UserFileForm extends CFormModel
{
    public $file;

    public function rules()
    {
        return array(
            array('file', 'required'),
        );
    }

    public function attributeLabels()
    {
        return array();
    }
}
