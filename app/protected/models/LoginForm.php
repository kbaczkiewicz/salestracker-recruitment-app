<?php

class LoginForm extends CFormModel
{
	public $username;
	public $password;
	public $rememberMe;

	public function rules()
	{
		return array(
			array('username, password', 'required'),
			array('rememberMe', 'boolean'),
		);
	}

	public function attributeLabels()
	{
		return array(
			'rememberMe'=>'Remember me next time',
		);
	}
}
