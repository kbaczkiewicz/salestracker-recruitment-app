<?php

/**
 * This is the model class for table "tbl_user".
 *
 * The followings are the available columns in table 'tbl_user':
 * @property integer $id
 * @property string $username
 * @property string $name
 * @property string $surname
 * @property string $password
 * @property string $date_of_birth
 * @property string $last_password_change_date
 * @property string $created_at
 * @property string $email
 */
class User extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tbl_user';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		return array(
			array('username, name, surname, password, date_of_birth, email, created_at, last_password_change_date', 'required'),
			array('username, password, email', 'length', 'max'=>128),
			array('name, surname', 'length', 'max'=>64),
			array('password', 'length', 'min'=>8),
			array('email', 'email'),
			array('date_of_birth', 'date'),
			array('last_password_change_date', 'date'),
			array('last_password_change_date', 'isExpired', 'days'=>7),
			array('id, username, email', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
    {
		return array(
		);
	}

    public function email()
    {
        if (null === $this->email) {
            return;
        }

        if ( 1 !== preg_match('/[a-zA-Z0-9]{1,}@[a-zA-Z]{1,}.[a-zA-Z]{2,3}/', $this->email)) {
            $this->addError('email', 'Email format is invalid');
        }
	}

    public function date()
    {
        if (null === $this->date_of_birth) {
            return;
        }

        if (false === \DateTime::createFromFormat('Y-m-d', $this->date_of_birth)) {
            $this->addError('date_of_birth', 'Date is incorrect');
        }
	}

    public function isExpired($attribute, $params)
    {
        if (null === $this->last_password_change_date) {
            return;
        }

        $interval = 'P' . $params['days'] . 'D';
        if (false === $this->checkLastPasswordChangeDate($interval)) {
            $this->addError('last_password_change_date', 'Password is expired');
            $this->addError('password', 'Password is expired. Check your inbox for an email with instructions on how to reset your password.');
        }
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'username' => 'Username',
			'name' => 'Name',
			'surname' => 'Surname',
			'email' => 'Email',
			'date_of_birth' => 'Date of birth',
			'created_at' => 'Created at',
			'last_change_password_date' => 'Last change password date',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('username',$this->username,true);
		$criteria->compare('name',$this->username,true);
		$criteria->compare('surname',$this->username,true);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('date_of_birth',$this->date_of_birth,true);
		$criteria->compare('created_at',$this->date_of_birth,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return User the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

    private function checkLastPasswordChangeDate(string $interval): bool
    {
        $dt = DateTime::createFromFormat(
            'Y-m-d',
            $this->last_password_change_date
        );

        $dt->add(new \DateInterval($interval));

        return null === $this->last_password_change_date || $dt > new \DateTime();
    }
}
